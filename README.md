Le projet `Fondations` regroupe les documents publics de Codeurs en Liberté : statuts, compte-rendus, modèles de contrat, guides des opérations courantes, etc.

- Les **compte-rendus, statuts, modèles** sont [dans le dépot git](https://gitlab.com/CodeursEnLiberte/fondations).
- La **documentation** sur nos valeurs et notre fonctionnement sont [dans le wiki](https://gitlab.com/CodeursEnLiberte/fondations/wikis/home).
- Les **discussions** sur la technique, sur l’administratif, ou sur les envies ont lieu [dans les issues](https://gitlab.com/CodeursEnLiberte/fondations/issues).

Voir aussi [l’annuaire des services](https://gitlab.com/CodeursEnLiberte/fondations/wikis/Outils/annuaire).
