# Discussions sur les ambitions / les envies / la pollinisation / le recrutement

_Date : 11 octobre 2018_

_Présents : Tristram, Thimy, Françis, Vincent, Nicolas, Étienne, Pierre_

## Tour de table initial

### **Envies** en vrac exprimées par certain.e.s des participant.e.s

- Plus de coopératives
    - Montrer que le modèle est réplicable
    - Sortir des gens de la subordination
- Lancer des actions concrètes : produit, conférences, etc
    - Faire du produit
    - Créer des communs utiles
- Sortir du productivisme
- Aider à l’insertion professionnelle
- Augmenter la force financière pour pouvoir faire plus de dons
- Participer concrètement aux projets qu’on finance
- Faire des goodies

### **Craintes** exprimées pendant le tour

- Stagner ; se mettre à ronronner ; perdre le sens de notre action
- Être trop nombreux ; ne plus connaître tous les Codeurs en Liberté
- Être éloigné des autres parce qu’éloigné géographiquement, ou parce que pas le même métier
- Que des dons d’argent suffisent à racheter notre conscience

## Discussion sur les dons

- Envies de continuer à faire des dons à des projets intéressants.
- Envies de le valoriser, de dire ce qu’on fait, pour inciter d’autres boîtes à faire pareil.
- Est-ce qu’on veut encourager les dons dans des domaines non liés à l’informatique ?

**Décisions :**

- **On continue à examiner les propositions de dons au cas-par-cas.**
- **Dons dans des domaines non-IT : pas de veto**, mais on voit au cas par cas.
- **Écrire un article de blog** sur notre système de dons.

## Discussion sur le produit

Envies de développer un produit propre à Codeurs en Liberté.

**Pourquoi ?**

- Pour travailler ensemble sur un même projet
- Pour faire une réalisation concrète
- Pour se poser des questions techniques

**Difficultés possibles**

- Trouver toutes les compétences
- Dégager du temps disponible à plusieurs en même temps
- Mettre à jour notre modèle de salariat pour permettre ça

_Pas de décision concrète pour l’instant._

## Discussion sur une conférence

Idée qui émerge : organiser « Code-code Coop », une conférence de coopératives dans l’informatique.

Ça permettrait de regrouper plusieurs envies (produit, pollinisation, faire des choses ensemble, etc).

On peut demander à Laurent de beta.gouv des conseils sur l’organisation de conférences.

**Décision**

- **Organiser une conférence de coopératives** cools, pour se rencontrer entre nous
    - Avec des coops d’indépendants, des coops de produit, etc.
- On invite des étudiants / jeunes pros / gens en reconversion-insertion
- On présente un autre modèle où la subordination n’est pas inévitable
- C’est un projet à réaliser ensemble entre nous

## Discussion sur l’essaimage / le recrutement

Pour favoriser la création de coopératives similaires à Codeurs en Liberté, on évoque l’essaimage : une fois qu’on devient suffisamment gros, une partie des gens créent une structure ailleurs. Ça permet de mettre le pied à l’étrier sans avoir à créer sa structure d’abord (vu que ça se fait ensuite).

- Crainte que le moment de l’essaimage soit difficile.
- Questionnements sur un essaimage dès aujourd’hui avec le groupe des Entrepreneurs d’Intérêt Général
- Tension entre la peur de stagner et la peur de grossir
- L’essaimage pourrait se produire de toute façon, il vaut peut-être mieux qu’il se produise en étant nombreux que peu
- Interrogations sur l’augmentation de la charge administrative dans une coop de 15-20 personnes
- Proposition d’une structure qui chapeauterait plusieurs coops, tout en gardant le même Mattermost
- Proposition de se mettre à aller chercher nous-mêmes des gens chouettes, et de leur proposer de rentrer

**Décisions**

- **Aller chercher des gens chouettes**
- Si possible des gens apportants une **plus grande diversité** que la majorité d’entre nous (métier / âge / genre / religion / racisation)
- **Ne pas recruter trop vite** ou en groupe

## Discussion sur le nom de la coopérative

On a déjà discuté sur les problèmes du nom « Codeurs en Liberté » les jours précédents (trop lié à un métier, trop masculin). Des alternatives émergent, mais pas encore mûres.

**Décision**

- **Écrire un article de blog sur les difficultés que nous pose notre nom**, et expliquer qu’on travaille à trouver mieux.
