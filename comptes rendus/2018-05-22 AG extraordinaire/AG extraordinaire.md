Assemblée générale extraordinaire n°3

# 30 mai 2017

Présents : Pierre, Thimy, Nicolas, Vincent, Tristram, Kheops.

L’ensemble des sociétaires étant présents, une assemblée générale spontanée est organisée.

Début de l’assemblée générale : 15 h 10 heure de Paris.

## Résolution numéro 1

Les employés Francis et Étienne souhaitent devenir sociétaires, à raison de 100 parts chacun.

L’assemblée se prononce sur leur acceptation :

* Pour : 6
* Contre : 0 
* Abstention : 0 

Résolution adoptée à l’unanimité.

Applaudissements et réjouissances.

## Résolution numéro 2

La dénomination de l’entreprise selon les statuts est modifiée comme telle : « Société coopérative par actions simplifiée à capital variable. »

L’assemblée se prononce sur cette résolution :

* Pour : 8
* Contre : 0 
* Abstention : 0

## Fin de l’assemblée générale extraordinaire

L’ensemble des points étant épuisés l’assemblée générale est levée à 15 h 38.
