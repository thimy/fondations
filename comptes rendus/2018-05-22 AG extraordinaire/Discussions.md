https://mensuel.framapad.org/p/uivH1YNpZP

#Journée coop Codeurs en Liberté 2018-05-22

Offycare (36 rue du Caire)
La Paillasse
~~Pour le soir, s'il fait beau, on pourrait aussi faire un pique-nique-b🍻🍻-🍹 dans un endroit chouette (Buttes-Chaumont, parc de Belleville, pont des Arts…)~~
Apéro à L’art Source et Diner à Presto Fresco

##Sujets

✔ Passage en cooperative
 * paragraphe dans les statuts
 * PR
 * validé en AGE
✔ Entrée des “nouveaux” membres au capital
✔ Trancher pour un code de déontologie
✔ IKV / indemnité 100€
✔ responsabilité civile
✔ L’appel d’offre DINSIC
 * \url{https://gitlab.com/CodeursEnLiberte/fondations/issues/48}

✔ Discuter de l'infrastructure des serveurs de CeL
 * augmenter le bus factor, est-ce qu'on veut toujours migrer sur "un VPS par service", comment s'y prendre, etc.

🍺Avancer au moins sur un point de nos valeurs histoire d’initier la cristallisation

 ---
  
##Discussion sur le code de déontologie

\url{https://gitlab.com/CodeursEnLiberte/fondations/issues/28}
\url{https://annuel2.framapad.org/p/iq3FJDGILE}

Nicolas : le sujet principal est la responsabilité du développeur. L’ACM (association d’informatique américaine) a fait un premier jet en 1992, mais ils préparent un draft 3 pour 2018.
Il y a deux aspects :
    * Travailler pour le bien
    * Soyez des bons professionnels : on ne pense que ce n’est pas trop le sujet car c’est déjà largement discuté, et c’est une question plus « commerciale »
    
Question : est-ce qu’on fait quelque chose pour nous, ou pour les développeurs en général ?

Discussion sur les contradiction possibles (si on refuse l’armée, est-ce Alan Turing aurait-il pu travailler ?)

Note: différence entre la charte et nos valeurs -> Nos valeurs décrivent comment on s'organise et pourquoi, et la charte précise comment on réalise notre travail. Une de nos valeurs par exemple peut-être de travailler éthiquement, ce qui est précisé par la charte. Par exemple, le fait de travailler moins correspond à nos valeurs, mais ne rentre pas dans la charte.

Code de déontologie : \url{https://annuel2.framapad.org/p/iq3FJDGILE}

Next step avec le code :
    * Mettre au propre dans un repo git
    * faire relire / commenter par tout le monde
    * Valider et publier


##AG extraordinaire

Cf compte rendu: ajout de Francis et Etienne en tant que sociétaires, ajout de la mention “coopérative” dans les statuts.

##Pot à Cookies

* Application du taux progressif: à partir de avril 2018

##Discussions diverses

* Rémunération solidaire
* Documentation des services sur le wiki
* Participation aux conférences, précision des modalités (cf wiki)

##Indemnité forfaitaire de déplacement

Aujourd’hui 100€, fixe, pour simplifier
* On pourrait faire un calcul plus complexe en additionnant navigo (35€), IKV (18€), repas (5€ par repas, mais uniquement les jours travaillés)
* On avait regardé un système de ticket resto l’an dernier et abandonné devant la complexité.

##Assurance Responsabilité Civile

\url{https://gitlab.com/CodeursEnLiberte/fondations/issues/39}
* utile pour des devis chez certains clients
 * pour les accidents/incidents faits chez des clients
 * pour faire face à des procès de clients
* pas très cher: 200/300€ par an
* pas forcément valable pour la sécurité informatique
* pas forcément valable au canada

Action; Francis relance l’assurance, y compris avec les question d’assurance juridique et de couverture au canada, et on suit.

##Appel d’offre de la DINSIC

(Présentation du contexte général de la dinsic, de betagouv et de l’appel d’offre)
Discussion sur les risques vis-à-vis de l’esprit de CeL

Questions bloquantes:
* Vincent, Etienne: pourra-t-on se désengager? Que risque-t-on si on ne fournit plus aucune UE?
* Tristram: Peut-on se désengager avant 4 ans
* Kheops: inquiétudes philosophiques, gêné à l’idée de faire de la sous-traitance

Kheops: qu’on réponde ou non, le système actuel de sous-traitance ne changera pas, mais autant le faire nous pour améliorer l’état.
Vincent: assez cool à l’îdée de faire des trucs au delà de notre cercle compétence, gros doute sur la capacité à tenir ça sur 4 ans, et sur la capacité à faire des trucs à distance avec d’autres sociétés
Etienne: plus gros risque pour CeL depuis le début; beaucoup d’énergie pour ce projet, “on pourrait avec cette énergie faire d’autre projets”
Thimy: envie d’y aller, pas sûr de connaître bien ce genre de sujet,
Tristram: envie, de démontrer que c’est possible et qu’on a peur d’un risque administratif financer. On n’arrive pas à se bouger sur un projet (cf pot à cookies), là il y a une deadline pour se forcer à faire des choses avec les autres, de créer des rituels. Inquiet de pas pouvoir continuer à bosser sur le projet actuel, mais OK, pas grave.
Pierre: voit les envies d’y aller et de faire mieux qu’octo, mais pas sûr d’avoir envie de collectivement mettre de l’énergie là-dedans
Francis: voit les envies, “je suis du genre à me noyer dans les gouttes”, ça me parait énorme et je n’aurai pas le temps de m’occuper de ça; peur du travail que ça représente et de la 
Nicolas: indécis, n'a pas envie dans l'immédiat de se lancer dans un projet d'une telle envergure

Actions: Voir effectivement:
Jeudi chez /ut7:
* Qui dans les autres structures est vraiment motivé? Qui “compte sur les autres”?
* Qu’est-ce que ça implique si on n’est plus motivé dans 18 mois? (pour nous comme pour la dinsic)
 * \url{https://gitlab.com/CodeursEnLiberte/18\_bam\_181/issues/4#note\_74396507}

* Aller voir Octo pour négocier la situation actuelle

##Bus Factor
