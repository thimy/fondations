# Assemblée générale ordinaire

Mardi 5 mars 2019

**Présents** :

* Vincent Lara
* Pierre de la Morinerie
* Nicolas Bouilleaud
* Thimy Kieu
* Francis Chabouis
* Tristram Gräbener
* (Antoine Desbordes)
* (Thibaut Sailly)

**Procurations** :

* Etienne Maynier: procuration à Thimy Kieu
* Aurélien Merel: procuration à Tristram Gräbener

**Début de scéance** :

Mardi 5 mars 2019 à 19h45

## Présentation du rapport de gestion

### Validation des comptes

Résolution soumise au vote :

* Pour 👍 8
* Contre 👎 0
* Abstention 😶 0

### Affectation du résultat

Résolution soumise au vote :

* Pour 👍 8
* Contre 👎 0
* Abstention 😶 0

### Quitus de gestion pour le président

Résolution soumise au vote :

* Pour 👍 8
* Contre 👎 0
* Abstention 😶 0

### Élection à la présidence

Candidat·e·s : Pierre de La Morinerie

Pierre est élu à l’unanimité.

La rémunération pour l’activité de présidence est fixée à 0 €.

### Création de nouvelles parts sociales

* Émission de 1 000 parts (1 € par part), soit 1 000 € pour Antoine Desbordes
* Émission de 100 parts (1 € par part), soit 100 € pour Thibaut Sailly
* Émission de 900 parts (1 € par part), soit 900 € pour Nicolas Bouilleaud
* Émission de 900 parts (1 € par part), soit 900 € pour Thimy Kieu

Résolution soumise au vote :

* Pour 👍 8
* Contre 👎 0
* Abstention 😶 0

### Cession de la propriété intellectuelle

* Codeurs en Liberté renonce à exiger tout droit sur Ubikiwi (marque, code source, exploitation) qui reste la propriété de Francis Chabouis.
* Codeurs en Liberté renonce à exiger tout droit sur Le Baby (marque, code source, exploitation).

* Pour 👍 8
* Contre 👎 0
* Abstention 😶 0

## Fin de séance

L’ensemble de l’ordre du jour ayant été épuisé, l’assemblée générale est levée à 21h16.

Les sociétaires décident de manger des crêpes.