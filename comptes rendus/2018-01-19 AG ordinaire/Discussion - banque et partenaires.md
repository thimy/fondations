# 1. Banque

## Situation actuelle

* Ibanfirst et BRED
* BRED:
 * très lente à réagir, même si _notre conseillère_ est sympa
 * Cher (700€ en 2017) (0.3% de frais de mouvement (quand l’argent sort))
* IbanFirst:
 * sert à économiser les frais de change (50€ grosso modo par mois)
 * futur incertain
 * bugs bizarres sur le site

## Pistes de changement

* Qonto
 * Prix intéressant 
 * 10 € par mois, puis 50c /virement au delà de 10 virements
 * Comptes en france
 * Virement SEPA b2b
  * Pratique pour payer les impôts et autres
 * On est la cible du truc
 * Permet d’avoir une carte par utilisateur
 
## Questions pour décision?

* Est-ce que 700€ c’est trop?
 * Qu’est-ce qu’on peut négocier à la BRED?
* Si on veut un local, la BRED peut faire garantie bancaire, pas Qonto.
* La BRED peut faire de l’affacturation, si on veut facturer l’état.
* La BRED est une banque mutualiste

## Critères de décision

* Négociation avec notre conseillère pour réduire les frais de virement et de change
* Accès au compte utilisateur pour tout le monde.

## Pistes abandonnées

* Tout passer sur qonto
 * Pas d’enthousiasme réel
* Volonté de simplifier et d’avoir une seule banque

## Actions

- Renégocier les tarifs de la BRED (@Tristram)
- Donner accès à tout le monde
- Si on décide d’y rester, prendre une part sociale


---

# 2. Complémentaire santé et mutuelle

## Situation actuelle

* Alan ne correspond pas vraiment aux valeurs 1/ avancées par CeL et 2/ acquis sociaux. Pour faire court, c’est une assurance santé capitalistique, pas une mutuelle.
* Pour la partie santé, Alan ne fait pas de revente d’autre assurance, ils sont (relativement) indépendants, même si non mutualistes.
* Alan est plutôt moins cher / rembourse moins bien que d’autres
 * mais rapide et simple
* Toutes les autres mutuelles et assurances sont plus complexes, leurs tarifs et conditions sont opaques
* Pas de bus-factor

## Bilan

* Côté remboursement: Alan, apparemment, couvre moins bien que d’autres.
* Côté outillage: imbattable
* Côté éthique: Alan n’est sans doute pas la pire entreprise privée non plus.

## Actions

* Dégrossir la situation: voir quelles mutuelles seraient bien pour nous, quelle image? quelles garanties et tarifs? (@vincent)
 
