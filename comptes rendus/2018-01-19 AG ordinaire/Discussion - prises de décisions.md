# Améliorer les prises de décision

## Situation actuelle

- On est très long à décider de quoi que ce soit. (Par exemple, les chèques culture.)
- Deux types de décisions et discussions: 
 - tout venant, souvent fermées par manque d’intérêt
 - issues importantes

## Actions

- Formaliser la gesion des issues
 - désigner un responsable
 - mettre une échéance

- Recentrer le Tour de table
 - Il sert à prendre les décisions sur les sujets en cours de CeL, pas uniquement à discuter la mission de chacun.
 - Faire le tour des issues lors du tour de table
 - Nouvel horaire:
  - jusqu’en mars 2018, à 11:30 le jeudi (KheOps est en france)
  - adaptable à mesure
 - Désigner un MC pour la discussion
  - le deuxième arrivé est le MC
