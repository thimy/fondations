# Avenant de mise en conformité à l’accord de participation

Entre

* d’une part la société, _Codeurs en Liberté_ représentée par Nicolas Bouilleaud, agissant en qualité de Président,
* et d’autre part les membres du personnel ayant ratifié à l’unanimité le projet lors de l’assemblée générale du 5 mars 2019,

il a été conclu le présent avenant. Les dispositions du présent avenant à l’accord de participation viennent se substituer à l’accord de participation initial dans sa totalité.

Les dispositions règlementaires peuvent être consultés sur :

* https://www.service-public.fr/particuliers/vosdroits/F2141
* https://www.urssaf.fr/portail/home/employeur/calculer-les-cotisations/les-elements-a-prendre-en-compte/lepargne-salariale/la-participation-des-salaries-au.html

## Article 1 : Dispositions générales

La société _Codeurs en Liberté_, bien qu’employant habituellement moins de cinquante salariés, a décidé de faire participer son personnel aux résultats de l’entreprise.

La participation est liée aux résultats de l’entreprise. Dès lors, elle existe dans la mesure où ces derniers permettent de dégager une réserve de participation positive. Celle-ci étant de fait aléatoire, les sommes pouvant être ainsi perçues ne sauraient donc constituer un avantage acquis pour les salariés.

Le présent accord a pour objet la fixation de la nature et des modalités de gestion des droits que les membres de l’entreprise auront au titre de la réserve spéciale de participation constituée à leur profit.

## Article 2 : Calcul de la réserve spéciale de participation

La somme attribuée à l’ensemble des salariés bénéficiaires au titre de chaque exercice est appelée réserve de participation. Elle est calculée selon la formule suivate :

   RSP = 1⁄2 B

Formule dans laquelle :
* RSP : Réserve Spéciale de Participation
* B : Bénéfice réalisé en France métropolitaine et les départements d'outre-mer, tel qu'il est retenu pour être imposé au droit commun de l'impôt sur les Sociétés. Ce bénéfice est diminué de l'impôt correspondant.

Cette formule ne s’applique que si elle se trouve globalement au moins aussi avantageuse pour les salariés que la formule légale, conformément à l’article L.-3324-2.

Ce montant est plafonné à la moitié du bénéfice net fiscal, conformément à L.-3324-2.

Ce montant peut être plafonné afin de respecter les plafonds collectifs et individuels tel que précisés dans les conditions règlementaires.

Les sommes attribuées individuellement sont plafonnées à trois-quart du plafond annuel de la sécurité sociale, proratisé en cas d’année incomplète.

## Article 3 : Salariés bénéficiaires

Les membres du personnel bénéficiant de la réserve spéciale de participation sont tous les salariés.

Le président ayant un contrat de travail, et n’étant pas rémunéré pour son activité de dirigeant bénéficie également de la réserve spéciale de participation.

## Article 4 : Répartition entre les bénéficiaires

La répartition est proportionelle à la durée de présence au cours de l’exercice.

Les périodes de suspension du contrat de travail (congés payés, heures de délégation et congés syndicaux, congés pour accident de travail et maladie professionnelle, congés de maternité ou adoption) légalement assimilés à des périodes de travail effectif, doivent être prises en compte pour totaliser la durée de présence choisie comme critèere de répartition.

Les sommes non-distribuées du fait de l’application des plafonds sont réparties entre tous les autres bénéficiaires dans la limite des plafonds, et ce jusqu'à épuisement du surplus.
Si un reliquat subsiste encore alors que tous les salariés ont atteint le plafond individuel, il demeure dans la réserve spéciale de participation des salariés et sera réparti au cours des exercices ultérieurs.

## Article 5 : Modalités de versement

Tout salarié bénéficiaire de l’intéressement pourra affecter tout ou partie de la part d’intéressement lui revenant au plan d’épargne d’entreprise.

Le salarié est présumé avoir été informé dons un délai de 15 jours à compter de la date d’envoi de l’avis d’option par email. Faute de réponse, un paiement direct sera effectué. 

En application de l’article R. 3324-21-1 du code du travail si le salarié ne demande pas le versement de ces sommes dans le délai de 15 jours à compter de la date à laquelle il est présumé avoir été informé, elles ne seront négociables ou exigibles qu’à l’expiration d’un délai de 5 ans à compter du 1er jour du 6ème mois suivant l’exercice au titre duquel les droits sont nés.

Les sommes inférieures à 80 € peuvent être versées immédiatement aux salariés.

Les sommes versées sur le plan épargne entreprise sont bloquées 5 ans, mais déblocables selon les dispositions règlementaires. Si la participation est bloquée, elle n’est pas soumise aux impôts sur le revenu. Les droits des salariés deviennent exigibles avant l’expiration du délai de 5 ans dans les cas suivants (article R3324-22 du Code du Travail)

1. Le mariage ou la conclusion d’un pacte civil de solidarité par l’intéressé ;
2. La naissance ou l’arrivée au foyer d’un enfant en vue de son adoption, dès lors que le foyer compte déjà au moins deux enfants à sa charge
3. Le divorce, la séparation ou la dissolution d’un pacte civil de solidarité lorsqu’ils sont assortis d’un jugement prévoyant la résidence habituelle unique ou partagée d’au moins un enfant au domicile de l’intéressé
4. L’invalidité de l’intéressé, de ses enfants, de son conjoint ou de son partenaire lié par un pacte civil de solidarité. Cette invalidité s’apprécie au sens des 2° et 3° de l’article L. 341-4 du code de la sécurité sociale ou est reconnue par décision de la commission des droits et de l’autonomie des personnes handicapées, à condition que le taux d’incapacité atteigne au moins 80 % et que l’intéressé n’exerce aucune activité professionnelle
5. Le décès de l’intéressé, de son conjoint ou de son partenaire lié par un pacte civil de solidarité
6. La rupture du contrat de travail, la cessation de son activité par l’entrepreneur individuel, la fin du mandat social, ou la perte du statut de conjoint collaborateur ou de conjoint associé
7. L’affectation des sommes épargnées à la création ou reprise, par l’intéressé, ses enfants, son conjoint ou son partenaire lié par un pacte civil de solidarité, d’une entreprise industrielle, commerciale, artisanale ou agricole, soit à titre individuel, soit sous la forme d’une société, à condition d’en exercer effectivement le contrôle au sens de l’article R. 5141-2, à l’installation en vue de l’exercice d’une autre profession non salariée ou à l’acquisition de parts sociales d’une société coopérative de production
8. L’affectation des sommes épargnées à l’acquisition ou agrandissement de la résidence principale emportant création de surface habitable nouvelle telle que définie à l’article R. 111-2 du code de la construction et de l’habitation, sous réserve de l’existence d’un permis de construire ou d’une déclaration préalable de travaux, ou à la remise en État de la résidence principale endommagée à la suite d’une catastrophe naturelle reconnue par arrêté ministériel
9. La situation de surendettement de l’intéressé définie à l’article L. 331-2 du code de la consommation, sur demande adressée à l’organisme gestionnaire des fonds ou à l’employeur, soit par le président de la commission de surendettement des particuliers, soit par le juge lorsque le déblocage des droits paraît nécessaire à l’apurement du passif de l’intéressé.

La demande du salarié doit être présentée dans un délai de six mois à compter de la survenance du fait générateur, sauf dans les cas de cessation du contrat de travail, décès du conjoint ou de la personne mentionnée au 5°, invalidité et surendettement ou elle peut intervenir à tout moment.
La levée anticipée de l’indisponibilité intervient sous forme d’un versement unique qui porte au choix du salarié, sur tout ou partie des droits susceptibles d’être débloqués.

Le jugement arrêtant le plan de cession totale de l’entreprise ou le jugement prononçant la liquidation judiciaire de l’entreprise rendent immédiatement exigibles les droits à participation non échus en application des articles L62l-95 et L622-22 du Code du Commerce et de l’article L3253-12 du Code du Travail.

Lorsque les bénéficiaires ont choisi de verser la participation sur l’une des formules de placement prévus dans Ie cadre du plan d’épargne entreprise la société doit réaliser le versement correspondant avant le premier jour sixième mois suivant la clôture der exercice suivant lequel la participation est attribuée. Le même délai de versement s’applique lorsque le salarié a opté pour le versement immédiat. Passé ce délai les entreprises complètent les versements prévus par un intérêt de retard égal à 1,33 fois le taux mentionné à l’article 14 de la loi n’47-1775 du 10 septembre 1947 portant statut de la coopération.

## Article 6 : Information des salariés

Le présent accord sera envoyé par courrier électronique à l’ensemble des salariés.

En outre, tout salarié bénéficiaire recevra, lors de chaque répartition, une fiche particulière sur laquelle figureront les éléments suivants :

* le montant de la réserve de participation constituée pour l’exercice écoulé,
* le montant des droits qui lui sont conférés et leur mode de gestion,
* la date à partir de laquelle ces droits sont négociables ou exigibles,
* les différentes hypothèses dans lesquelles ils peuvent être exceptionnellement liquidés ou transférés avant ce délai.

Au cas où un salarié quitte l’entreprise avant que l’entreprise n’ait liquidé la totalité de ses droits, il recevra une attestation se présentant sous la même forme que la fiche précitée, ainsi qu’un état récapitulatif de l’ensemble des sommes et valeurs mobilières épargnées ou transférées au sein de l’entreprise. Il en sera de même, si le salarié n’a pas demandé le déblocage anticipé de ses droits avant son départ. Dans ces deux hypothèses, l’entreprise devra demander l’adresse à laquelle le salarié pourra être joint. Si ce dernier change d’adresse ultérieurement, il lui appartiendra donc d’en avertir la société. Lorsqu'un salarié ayant quitté l'entreprise ne peut être atteint à la dernière adresse indiquée par lui, les sommes et droits auxquels il peut prétendre sont affectées au plan d’épargne. La conservation des fonds commun de placement continue d’être assurée par l’organisme qui en a la charge pendant dix ans puis les avoirs du bénéficiaire sont remis à la Caisse des Dépôts et Consignations qui les conserve pendant vingt ans. L’intéressé pourra les réclamer jusqu’au terme de la prescription.

Par ailleurs, chaque année, dans les six mois suivant la clôture de l’exercice, l’employeur est tenu de présenter au comité d’entreprise un rapport comportant notamment les éléments servant de base de calcul de la réserve spéciale de participation et des indications précises relatives à la gestion et à l’utilisation des sommes affectées à cette réserve.

## Article 7 : Suivi de l’accord

Le rapport relatif à l’accord de participation est adressé à chaque salarié présent dans l’entreprise à l’expiration du délai de six mois suivant la clôture de l’exercice.

Ce rapport comporte notamment

1. Les éléments servant de base au calcul du montant de la réserve spéciale de participation des salariés pour l’exercice écoulé
2. Des indications précises sur la gestion et l’utilisation des sommes affectées à cette réserve

## Article 8 : Contestations

En cas de litige individuel ou collectif relatif à l’interprétation ou à l’application du présent accord, les parties s’engagent à essayer de résoudre à l’amiable le litige qui les oppose, avant de recourir aux juridictions compétentes, à savoir celles du lieu du siège social de la société (Tribunal Administratif pour les litiges relatifs au montant des salaires ou au calcul de la valeur ajoutée et Tribunal d’Instance ou de Grande Instance pour les autres litiges).

À titre informatif, il est précisé, en outre, que le montant du bénéfice net étant attesté par l’inspecteur des impôts (ou par le commissaire aux comptes), il n’est pas contestable.

## Article 9 : Prise d’effet et durée de l’accord

Le présent avenant s’appliquera pour la première fois aux résultats de l’exercice ouvert le 1er janvier 2019 et clôturé le 31 décembre 2019.

Il est conclu pour une durée indéfinie.

Fait à Paris le 5 mars 2019
