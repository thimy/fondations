* Don à (Nom et URL)
* Montant : (Montant) €

### Est-ce utile pour le bénéficiaire ?
### Est-ce pertinent par rapport aux valeurs que Codeurs en Liberté veut pousser ?
### Est-ce qu’on a les moyens ?
### Est-ce qu’il y a un conflit d’intérêt avec un sociétaire ?


---

Étapes: 

* [ ] Discussion en point hebdomadaire
* [ ] Consensus 7 jours après la création de l’issue
* [ ] Don 💸

Le consensus s’entend par « au moins trois personnes se sont exprimées en faveur, et personne ne s’est opposé ».

/label ~Don
/due in 7 days
