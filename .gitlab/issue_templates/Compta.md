Titre: Compta de \<MOIS\>

[Voir le guide sur le wiki](../wikis/Administratif/Opérations-comptables-mensuelles):
* [ ] Comptabiliser la paye
* [ ] Faire la DSN :man\_in\_business\_suit\_levitating:
* [ ] Importer relevé bancaire BRED :bank:
* [ ] Importer relevé bancaire IbanFirst :bank:
* [ ] Importer relevé bancaire Crédit Mutuel :bank:
* [ ] Faire le rapprochement bancaire
* [ ] Déclarer et payer la TVA :memo: :money\_with\_wings: 
* [ ] Payer les sous-traitants _la zone_

Puis
* [ ] Créer l’issue pour le mois suivant :heart:


/label ~Administratif
/due in one month
/assign @codeursenliberte
